+++
title = 'Displaying complex information on retroconsoles'
date = 2024-07-23T03:26:39+02:00
draft = false
+++

... You can't do it, the screens are just too goddamn small!!! <!-- more -->

Way back when, when I had a little bit less experience, I tried making a roguelike game thing for the original Game Boy... But the goddamn thing has only 160x144 pixels! Combine that with the fact you have to use 8x8 tiles, that means 20x18 tiles OR 20x18 character tiles. There's literally just not enough space to display all the information. You pretty much have to use one screen tile per map tile otherwise you'll end up with pitful 10x9 map tiles per screen (without any text). Making 8x8 sprites look like literally anything quite a challange on its own, but ignoring that, you still only got 360 map tiles per screen, minus however much text you want to put on it. On even the original Rogue you'd have that many times over. In the original Rogue at any given time about 100-200 characters of the screen were reserved just for the HUD text information, while still only taking up negligible amount of space. If you used up 200 tiles of the Game Boy screen for HUD you'd only get enough space for about a 12x12 view of the map, some rooms in Rogue are larger than that.