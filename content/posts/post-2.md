+++
title = '... Moving to Hugo'
date = 2024-07-22T12:38:21+02:00
draft = false
+++

And as I (kinda) foretold, 2 months (and 0 posts) later, I am running into
limitations of Cobalt. <!-- more --> Cobalt, as far as i can tell, is entirely an SSG for
blogs, which I guess is what attracted me to it at first and stuff, but now I
wanted to do stuff past that and I find that I cannot. So hopefully Hugo will
be more accommodating. It is certainly more uh... "featureful." This just
speaks to the reality of ever changing requirements I guess.