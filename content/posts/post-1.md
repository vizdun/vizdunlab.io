+++
title = "Making This"
date = 2024-04-20T00:00:00+00:00
draft = false
+++

For the longest time, I didn't really care to make a blog, but what's the point
of misadventures if there's nobody to see them? <!-- more --> I mean, that's not the reason
I'm making this blog, but that's a nice idea.

So on the tech in this thing, it's very simple, it's very basic, a generated
static site on gitlab pages (f u github). I thought about using one of the more
established generators, like Jekyll or Hugo or whatever is the Python one but
then I didn't really feel like going down the Ruby legacy lane, or installing
Go (f u google, also all of Go's technical issues), or going to Python's
slugfest (though Python would probably still be faster than Ruby).

I am then forced to look to the one language I don't hold much grudge against,
Rust. What Rust static site generators are there you ask? It seems there are
mainly two: Zola and Cobalt (and mdBook, but that isn't very flexible and I
feel if I opened up somebody's blog and it looked like Rust library
documentation I'd die on the spot). I first tried Zola, it had lotsa stars and
seemed okay, however after using it for a bit (about 2 minutes) I came to the
conclusion it was overcomplicated and actually too flexible (if you came here
from a newer blog post where I say this was a grave mistake, sorry future me),
so I went with Cobalt in the end, which seems to be much more blog focused.

For the CSS, I ended up straight up just using Tufte CSS umodified, perhaps
I'll mod it later, but frankly, I'm not a web developer and it doesn't interest
me much.

Also I might just end up scrapping all of this once (or if) typst gets an HTML
backend, so that's fun.