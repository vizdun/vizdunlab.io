+++
title = 'Architecture of North Korean Internet Sites'
date = 2024-08-06T04:49:29+02:00
draft = false
+++

This was supposed to be a full blog post but I can't handle that much work so it's a thought, take it or leave it.<!-- more -->

It seems they're all built in one huge monorepo. They all use relatively the same tech:

* JQuery
* Bootstrap
* PHP 5.6.2
* Apache 2.4.25 or nginx/1.18.0
* OpenSSL/1.0.1e-fips
* Red Star 4.0 (!!!)

all relatively the same versions, all stuck in (early) 2010s.

The only exceptions to this stack are sdprk.org.kp and pyongyangtimes.com.kp which use Next.JS with Webpack, though still on the same Apache/PHP server.