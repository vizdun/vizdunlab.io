+++
title = 'Posts-Thoughts merger & new look'
date = 2025-01-30T16:44:04+01:00
draft = false
+++

Looking around I noticed that the posts-thoughts dichotomy barely makes sense. Even though thoughts were intended to be shorter less effort content I would only ever ended up making them, [and one of them](@/posts/thought-2.md), was actually the longest post on the whole site!

Hence I merged these two into a single directory.

I changed the way the site is layed out. The posts are now entirely readable from the listing (since they're so short anyway). CSS is new. There are those tiny cute dots at the bottom of each post indicating the post number.