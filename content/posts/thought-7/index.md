+++
title = 'Rust tooling woes'
date = 2025-01-28T03:42:00+01:00
draft = false
+++

So the bozos at Rust tooling are back with a new feature! Did they fix the "rust-analyzer randomly giving up on life" issue or rust-fmt doing the same? Nope! Instead they added a new, brand new, hardcoded logic for iterators. Where the godforsaken first suggestion is always `into_iter()` followed by the actual suggestion, even if the snippet already contains `into_iter()` right before!

![First suggestion on a snippet ending with `.into_iter().map` starts with `.into_iter().map()` instead of just `.map()`, pointlessly suggesting another `.into_iter()`](image.png)

Why????? Who has asked for this???? Even if it worked it's not like anybody cares!!! The amount of rage that fills me whenever I use iterators now is insane. I hate this so much.