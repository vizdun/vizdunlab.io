+++
title = 'Scuffed rewrite (of this)'
date = 2024-11-24T15:21:19+01:00
draft = false
+++

Okay so TL;DR: I found Hugo limiting/annoying and so I tried to write a custom SSG in Rust. But then I decided hated making SSGs, found out there's basically nothing I would want to add to one, and then switched to Zola instead. Expect the site to barely change.<!-- more -->