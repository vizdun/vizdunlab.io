+++
title = 'PayDay 2: Vampire the Masquerade Overhaul'
date = 2024-07-23T02:59:51+02:00
draft = false
+++

The latest of my VTM mod ideas and project tryings. <!-- more --> The idea was it'd be based on a weird mix of V20 & V5. Originally I thought I'd just add some skill trees & perk decks but then I of course had to go full deranged and eventually ended up wanting to do a whole overhaul of just about every mechanic out there. I don't think it's in the stars anymore as I can be a very lazy person and PayDay 2's codebase is not exactly welcoming, but still, for whoever might find this interesting, here is what was the plan.

## Skills and XP progression

It was going to be somewhat inspired from VTM - Bloodlines. I was going to scrap the entirity of PayDay 2 skill and perk systems and replace it with one more similar to Bloodlines CRPG skills thing. There was going to be an entirely new menu with the character sheet and the dots. 0-5 dots, influencing just about every action. Melee, gun combat, throwing bags, interaction speed, stealth, everything.

## Mechanics

The idea was to keep faithul to the TTRPG mechanics. That meant throwing out virtually all mechanics and systems and starting from scratch. We probably wouldn't roll dice pools for where it didn't make sense (like interactions), but could've for elsewhere (like combat).

## Difficulty

Difficulties were going to be all scrapped and only 1 difficulty would exist. However, to balance out the fact you were going to be a (relatively) powerful kindred against just a bunch of kine, there would be permadeath with no restarts. You had to play the heist how it is or leave it (in universe run away). There would be penalties in the form of lost *Favours*.

All apart from the somewhat reasonable cop units would be scrapped (dozers, tasers, cloakers, etc.). There was an idea for a ghoul unit or something of the sort.

## Currencies

Offshore and Spending money would be merged into one. Contracts would now be purchased using *Favours*. You would get favours in return for completing heists successfully, amount perhaps in relation to how well you did. You would lose favours for failing heists. Continental coins would be removed.

There was going some focus on the financial and sorta social aspect of this. Such as Skills that would improve heist payouts for the team. Similarly there would probably be some sort of Masquerade mechanic.

## State

I did write code for some of this, but frankly I kind of lost interest in PayDay 2, the codebase is pretty cancerous (especially without documentation), the UI code is particularly bad, and I am very lazy, so the chances of a mod like this seeing the light of day (or the dark of night) are very small.