+++
title = 'Preparatory Math Course Week 3: Trigonometry'
date = 2025-01-29T19:10:00+01:00
draft = false
+++

This week the subject was Trigonometry. This week the lecture was theoretical, next week we'll be solving trigonometry problems. Lecturer (different lecturer this time from the previous weeks) had a whole story from history prepared with how trigonometry came to be, how the functions sinus and tangens were originally named, much fun.

His story described trigonometry as first solving problems of ancient astronomers. He described the trigonometric functions as coming from India, through the Islamic world, before finally being named in the Christian world, where sinus conceptually referred to a fold on one's clothes, and tangens referred to something related to dancing.