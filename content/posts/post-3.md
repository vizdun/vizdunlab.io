+++
title = 'Introducing: Thoughts'
date = 2024-07-23T02:50:09+02:00
draft = false
+++

As alluded to in my previous post, I haven't been posting very much. <!-- more --> This, I
think, is at least in part due to the effort that needs to be expensed to write
one of these posts, plus also me rarely finishing anything to the extent it
where it ought to have a post. To this end, I am introducing: [*Thoughts*](@/posts/_index.md), short
form content consisting mostly of sporadic thoughts that didn't warrant an
actual blog post.