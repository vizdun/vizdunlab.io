+++
title = 'Masks in PayDay'
date = 2025-01-17T02:33:00+01:00
draft = false
+++

In PayDay 1 and 2 heisters start heists in "casing mode" (ie. without their masks on). They cannot do anything sus in casing mode, such as interacting with any objectives, or running, or anything like that. Once they mask up the game actually begins.

In PayDay 3, most of the heists can be stealthed (partially or completely) without putting the mask on. Which offers the benefit of not being detected by civilians.

The trouble is, that doesn't make any sense. Canonically and logically everybody knows the heisters, they're infamous, especially in PayDay 2. They can get their pictures, banks would certainly have them.

What even is the point of the mask? How can you stealth anything when they can just look at you and the cover is blown.