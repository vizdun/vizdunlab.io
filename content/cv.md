+++
title = "Short CV"
+++

Hi, I'm a computer programmer. I do a little bit of everything but lately I've been particularly interested in databases, database management systems, and query languages. Currently I am a part-time high school student.

## Contact

- Email: [lukas.snizek1@seznam.cz](mailto:lukas.snizek1@seznam.cz)
- GitLab: [https://gitlab.com/vizdun](https://gitlab.com/vizdun)

## Skills

- Programming languages:
    - Expert: Rust, Lua
    - Intermediate: C, SQL, HTML, CSS, JS/TS, Python
    - Marginal experience: Java, C++, C#, Haskell, Go, PHP, Ruby, Haxe, Dart, Gleam, Forth, Lisp, RDF, SPARQL, SurrealQL, openCypher
- Frameworks:
    - Rust: Rocket, Actix Web, Axum, Tokio, Diesel, sqlx
    - CSS: Tailwind
    - JS: React.js, Solid.js, Vue.js
- SSRs: Hugo.io, Zola
- Data formats: JSON, Toml, YAML, RON
- Markup languages: Typst, LaTeX, Markdown
- Databases: PostreSQL, SQLite, MySQL, SurrealDB, Neo4J
- Other services: Git, Docker, Docker-compose, Terraform/OpenTofu, NATS.io, RabbitMQ, Meilisearch, Nginx
- Operating systems: Linux
- Protocols: S3, HTTP/HTTPS
- Architectures: REST, CRUD, Monolith, Microservices, Serverless
- Natural languages: English, Czech

## Competitions

- Successful solver (11th overall) of the [36th year of KSP](https://ksp.mff.cuni.cz/h/ulohy/36/vysledky5.html)

## Projects

- [matdb (2024-)](https://gitlab.com/vizdun/matdb) - WIP SQL-like DBMS, subject of my matura thesis
- [datalibrary (2024)](https://gitlab.com/vizdun/datalibrary) - AI-powered PDF search software
- [vizchan (2022-2023)](https://gitlab.com/vizdun/vizchan) - imageboard software
- [tvmaze (2022)](https://gitlab.com/vizdun/tvmaze) - [TVMaze](https://www.tvmaze.com/) API wrapper library
- [ao3-rs (2022)](https://gitlab.com/vizdun/ao3-rs) - [Archive of Our Own](https://archiveofourown.org/) scraping library
- [showtracker (2021-2022)](https://gitlab.com/vizdun/showtracker) - CLI tool for tracking TV shows
- further graveyard of projects is available on my [GitLab](https://gitlab.com/vizdun)