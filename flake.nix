{
  description = "Zola custom build script coz everything's fucked and we're all gonna die";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";

  outputs = {nixpkgs, ...}: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    packages."x86_64-linux".default = with pkgs;
      rustPlatform.buildRustPackage rec {
        pname = "zola";
        version = "0.19.2-unstable-2025-01-19";

        src = fetchFromGitHub {
          owner = "getzola";
          repo = "zola";
          rev = "233d1b8c38299f2cf6ce11f931598af5ebd84a2f";
          hash = "sha256-BjHAHj3EGE1L+EQdniS4OGOViOmcRDR5RhgmYK2zmVY=";
        };

        cargoHash = "sha256-D/q580JPAMbumgCECpC6+06NkNcjSXCv51zD5yghk34=";

        nativeBuildInputs = [
          pkg-config
          installShellFiles
        ];

        buildInputs =
          [
            oniguruma
          ]
          ++ lib.optionals stdenv.hostPlatform.isDarwin (
            with darwin.apple_sdk.frameworks; [
              CoreServices
              SystemConfiguration
            ]
          );

        RUSTONIG_SYSTEM_LIBONIG = true;

        postInstall = lib.optionalString (stdenv.buildPlatform.canExecute stdenv.hostPlatform) ''
          installShellCompletion --cmd zola \
            --bash <($out/bin/zola completion bash) \
            --fish <($out/bin/zola completion fish) \
            --zsh <($out/bin/zola completion zsh)
        '';

        passthru.tests.version = testers.testVersion {package = zola;};

        meta = with lib; {
          description = "Fast static site generator with everything built-in";
          mainProgram = "zola";
          homepage = "https://www.getzola.org/";
          changelog = "https://github.com/getzola/zola/raw/v${version}/CHANGELOG.md";
          license = licenses.mit;
          maintainers = with maintainers; [
            dandellion
            dywedir
            _0x4A6F
          ];
        };
      };
  };
}
